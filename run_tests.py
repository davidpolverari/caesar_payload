#!/usr/bin/env python3

from xor_payload import *
from caesar_payload import *
import unittest

class TestEncryption(unittest.TestCase):
    def test_csharp_conversion(self):
        input_bytes = b'ABC'
        csharp_output = bytes_to_csharp(input_bytes)
        self.assertEqual(csharp_output, 'byte[] buf = new byte[3] { 0x41,0x42,0x43 };')

    def test_simple_encryption(self):
        input_bytes = b'\x00\x01\x02'
        encrypted_bytes = caesar_encrypt(input_bytes, 1)
        self.assertEqual(encrypted_bytes, b'\x01\x02\x03')

    def test_boundary_encryption(self):
        input_bytes = b'\x00\xff\x7f'
        encrypted_bytes = caesar_encrypt(input_bytes, 1)
        self.assertEqual(encrypted_bytes, b'\x01\x00\x80')

    def test_decryption(self):
        input_bytes = b'\x00\xff\x7f'
        offset = 2
        encrypted_bytes = caesar_encrypt(input_bytes, offset)
        decrypted_bytes = caesar_encrypt(encrypted_bytes, (offset * (-1)))
        self.assertEqual(input_bytes, decrypted_bytes)

class TestXOREncryption(unittest.TestCase):
    def test_encryption(self):
        key = b'any_key'
        input_bytes = b'this is a test'
        encrypted_bytes = xor_encrypt(input_bytes, key)
        cleartext = xor_encrypt(encrypted_bytes, key)
        self.assertEqual(input_bytes, cleartext)

unittest.main()
