# caesar\_payload

A simple Python3 script to encrypt raw payloads.

## Usage

```bash
caesar_payload <lang> <filename> <offset>
```

Supported languages are `csharp` and `vba`.
