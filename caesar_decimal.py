#!/usr/bin/env python3

import sys

def main():
    program = sys.argv[0]

    if len(sys.argv) < 3:
        print('%s: encrypts a string with Caesar cipher according with the offset passed, and outputs 3-digit decimal encoded payload' % program, file=sys.stderr)
        print('usage: %s <string> <offset>' % program, file=sys.stderr)
        sys.exit(1)

    try:
        offset = int(sys.argv[2])

    except ValueError:
        print('error: <offset> must be an integer', file=sys.stderr)
        sys.exit(1)

    payload = sys.argv[1]
    payload = payload.encode('utf-8')

    for ch in payload:
        print('%03d' % ((ch + offset) & 0xff), end="")

if __name__ == '__main__':
    main()
