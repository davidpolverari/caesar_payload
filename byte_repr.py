
def csharp_byte_repr(input_bytes):
    size = len(input_bytes)
    result = "byte[] buf = new byte[%d] { " % size

    for i, val in enumerate(input_bytes):
        result += '0x%02x%c' % (val, ',' if i != (size - 1) else ' ')

    result += "};"

    return result

def vba_byte_repr(input_bytes):
    size =len(input_bytes)
    result = "buf = Array("

    for i, val in enumerate(input_bytes):
        result += '%d%c ' % (val, ',' if i != (size - 1) else ' ')
        if (i+1) % 50 == 0:
            result += '_\n'

    result += ")\n"

    return result
