#!/usr/bin/env python3

import sys

from byte_repr import *

def csharp_caesar_decoder(offset):
    result = (
        "\n\nfor(int i = 0; i < buf.Length; i++)"
        "\n{"
        "\n\tbuf[i] = (byte)(((uint)buf[i] - %d) & 0xFF);"
        "\n}"
    ) % offset

    return result

def vba_caesar_decoder(offset):
    result = (
        "\n\nFor i = 0 to UBound(buf)"
        "\n\tbuf(i) = buf(i) - %d"
        "\nNext i\n"
    ) % offset

    return result

def caesar_encrypt(plain, offset):
    content = bytearray(plain)

    for i, val in enumerate(plain):
        content[i] = (val + offset) & 0xff

    return bytes(content)

def encrypt_file(filename, offset):
    with open(filename, 'rb') as f:
        payload = f.read()
        encrypted_payload = caesar_encrypt(payload, offset)
        return encrypted_payload

def main():
    program = sys.argv[0]

    if len(sys.argv) < 4:
        print('%s: encrypts a raw payload file with Caesar cipher and outputs C# or VBA buffer, along with decryption routine' % program, file=sys.stderr)
        print('usage: %s <lang> <filename> <offset>\n' % program, file=sys.stderr)
        print('supported langs: csharp, vba');
        sys.exit(1)

    lang = sys.argv[1]
    
    if lang == 'csharp':
        byte_repr = csharp_byte_repr
        decoder_routine = csharp_caesar_decoder
    elif lang == 'vba':
        byte_repr = vba_byte_repr
        decoder_routine = vba_caesar_decoder
    else:
        print('error: "%s" is not a supported language', lang, file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[2]

    try:
        offset = int(sys.argv[3])

    except ValueError:
        print('error: <offset> must be an integer', file=sys.stderr)
        sys.exit(1)

    try:
        encrypted = encrypt_file(filename, offset)

    except FileNotFoundError:
        print('error: the file does not exist', file=sys.stderr)
        sys.exit(1)

    output = byte_repr(encrypted)
    decoder = decoder_routine(offset)
    output += decoder

    print(output)

if __name__ == '__main__':
    main()
